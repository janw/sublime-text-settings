# Sublime Text settings

This repository features my personal flavour of Sublime Text customizations I work with. I tend to keep it pretty clean and install very few packages. Nonetheless, I want to the settings synced.

## Setup process

1. Install [`Package Control`](https://sublime.wbond.net/installation)
2. Install [`Package Syncing`](https://sublime.wbond.net/packages/Package%20Syncing)
3. Clone this repository
4. `Define Sync Folder` from Sublime's Command Palette

# Licence

Feel free to fork/copy the contents of this repository as you like. I hereby place it in public domain.